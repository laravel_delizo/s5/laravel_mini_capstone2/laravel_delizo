@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-body">
    <h2 class="card-title">{{$post->title}}</h2>
    <p class="card-text text-muted">Author: {{$post->user->name}}</p>
    <p class="card-text text-muted">Likes: {{$post->likes->count()}}</p>
    <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
    <p class="card-text">{{$post->content}}</p>
    {{-- Check if the user is logged in --}}
    @if(Auth::user())
    {{-- Check if the post author is NOT the current user --}}
    @if(Auth::user()->id != $post->user_id)
    <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
      @method('PUT')
      @csrf
      {{-- Check if the user has already liked the post --}}
      @if($post->likes->contains("user_id", Auth::user()->id))
      <button type="submit" class="btn btn-danger">Unlike</button>
      @else
      <button type="submit" class="btn btn-success">Like</button>
      @endif
    </form>


    @endif
    @endif

    <div class="mt-3">
      <a href="/posts" class="card-link">View all posts</a>
    </div>
  </div>

  @if(count($post->comments) > 0)
  <h4 class="mt-5">Comments:</h4>
  <div class="card">
    <ul class="list-group list-group-flush">
      @foreach($post->comments as $comment)
      <li class="list-group-item">
        <p class="text-center">{{$comment->content}}</p>
        <p class="text-end text-muted">posted by: {{$comment->user->name}}</p>
        <p class="text-end text-muted">posted on: {{$comment->created_at}}</p>
      </li>
      @endforeach
    </ul>
  </div>
  @endif
</div>






{{-- Check if the post author is NOT the current user --}}

<form method="POST" action="/posts/{{$post->id}}/comment">
  @csrf
  {{-- Check if the user has already liked the post --}}
  <div class="form-group">
    <label for="content">Content:</label>
    <textarea class="form-control" id="content" name="content" rows="3" required></textarea>
  </div>
  <button type="submit" class="btn btn-danger">Post comment</button>
</form>


@endsection
